import { Component } from '@angular/core';
import { BlockService } from '@base/control/base-block/block.service';
import { DialogService } from '@base/control/base-dialog/dialog.service';
import { FloatService } from '@base/control/base-float/float.service';
import { ExcelMap } from '@base/models/excel-map.model';
import { BaseComponent } from '@core/BaseComponent';

@Component({
  selector: 'app-root',
  template: `<router-outlet></router-outlet>`,
  styles: [],
})
export class AppComponent extends BaseComponent {}
