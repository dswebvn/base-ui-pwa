import { Component, ElementRef, EventEmitter, Input, Output } from '@angular/core';
import { BlockService } from './block.service';

@Component({
  selector: 'base-block',
  template: `
    <div class="block-ui" [ngClass]="{ 'on-loading': isLoad }" [class]="classBlock">
      <ng-content></ng-content>
      <div class="loading">
        <div class="ldr">
          <div class="ldr-blk"></div>
          <div class="ldr-blk an_delay"></div>
          <div class="ldr-blk an_delay"></div>
          <div class="ldr-blk"></div>
        </div>
      </div>
    </div>
  `,
  styles: [
  ],
  styleUrls: ['./base-block.component.scss']
})
export class BaseBlockComponent {
  private _element: any;
  @Input('load') isLoad:boolean = false;
  @Input() id: string | any;
  @Input('bclass') classBlock: string | any;
  @Output() onInit = new EventEmitter(); // Handle sự kiện khi block được load xong

    
  constructor(public blockService:BlockService, private el: ElementRef) {
    this._element = el.nativeElement;
   }

  ngOnInit(): void {
    this.blockService.add(this);
    this.onInit.emit();
  }

  ngOnDestroy(): void {
    this.blockService.remove(this.id);
    this._element.remove();
  }

  load(): void {
   this.isLoad = true;
    // document.body.classList.add('on-loading');
  }

  unload(): void {
    this.isLoad = false;
    // document.body.classList.remove('on-loading');
  }
}
