### HƯỚNG DẪN SỬ DỤNG BLOCK

---

```html
<base-block id="demo">
      Nội dung
</base-block>
```
```ts
export class AppComponent {
 constructor(private blockService:BlockService){
 }

 load()[
    this.blockService.load("demo")
 ]
}
```

| Input      | mô tả                                    | kiểu dữ liệu   | ví dụ      |
| ---------- | ---------------------------------------- | -------------- | ---------- |
| load       | trạng thái mặc định loading              | boolean        | true       |
| id         | ID định danh dùng để gọi                 | string         | 'demo'     |
| bclass     | Set class cho block                      | string         |            |

| Sự kiện | mô tả                             |
| ------- | --------------------------------- |
| onInit  | Kích hoạt khi block khởi tạo xong |

| Hàm    | mô tả        |
| ------ | ------------ |
| load   | bật loading  |
| unload | hủy loadding |