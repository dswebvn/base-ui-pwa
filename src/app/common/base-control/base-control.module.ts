import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { CommonModule } from '@angular/common';
import { MatIconModule } from '@angular/material/icon';
import { MatDialogModule } from '@angular/material/dialog';

import { BaseBlockComponent } from './base-block/base-block.component';
import { BaseFloatComponent } from './base-float/base-float.component';
import { BasePrintComponent } from './base-print/base-print.component';
import { BaseDialogComponent } from './base-dialog/base-dialog.component';

import { BaseStepComponent } from './base-step/base-step.component';
import { BaseUploadComponent } from './base-upload/base-upload.component';
import { BasePagingComponent } from './base-paging/base-paging.component';
@NgModule({
  declarations: [
    BaseBlockComponent,
    BaseFloatComponent,
    BasePrintComponent,
    BaseDialogComponent,
    BaseStepComponent,
    BaseUploadComponent,
    BasePagingComponent,
  ],
  imports: [CommonModule, MatDialogModule, MatIconModule],
  exports: [
    BaseBlockComponent,
    BaseFloatComponent,
    BasePrintComponent,
    BaseDialogComponent,
    BaseStepComponent,
    BaseUploadComponent,
    BasePagingComponent,
  ],
})
export class BaseControlModule {}
