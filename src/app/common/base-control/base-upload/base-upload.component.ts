import { HttpEvent, HttpEventType } from '@angular/common/http';
import {
  Component,
  ElementRef,
  EventEmitter,
  Input,
  Output,
  ViewChild,
} from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { BaseDialogComponent } from '../base-dialog/base-dialog.component';
import { FileObj } from '@base/models/file-obj.model';
import { FileJS } from '@base/models/file.model';

@Component({
  selector: 'base-upload',
  templateUrl: './base-upload.component.html',
  styleUrls: ['./base-upload.component.scss'],
})
export class BaseUploadComponent {
  isHighlight: boolean = false;
  filePendings: FileObj[] = [];
  fileUploads: FileJS[] = [];
  autoId: string = 'upload_' + Math.random().toString(36).substring(2);
  @ViewChild('controlFile') controlFile: ElementRef | any;
  @Output('done') done: EventEmitter<any> = new EventEmitter();
  @Input() multiple: boolean = true;

  @Output('upload') upload: EventEmitter<any> = new EventEmitter();
  @Output('remove') remove: EventEmitter<any> = new EventEmitter();

  constructor(
    // private uploadService: UploadService,
    // private toastr: ToastrService,
    public dialog: MatDialog
  ) {}

  ngOnInit(): void {}

  init(): void {
    this.isHighlight = false;
    this.filePendings = [];
    this.fileUploads = [];
    this.autoId = 'upload_' + Math.random().toString(36).substring(2);
  }

  uploadFile(file: Blob) {
    let reader = new FileReader();
    var self = this;
    reader.readAsDataURL(file);
    reader.onloadend = function () {
      // self.preview = reader.result; //.push(reader.result);
      //let result = { file: file, preview: reader.result };
      self.doUpload(file, reader.result);
    };
  }

  doUpload(file: any, preview: string | ArrayBuffer | null) {
    let fileObj = new FileObj(file);
    if (typeof preview == 'string')
      if (preview.includes('image')) fileObj.preview = preview;

    this.filePendings.push(fileObj);
    fileObj.start();
    this.controlFile.nativeElement.value = '';
    this.upload.emit({
      file,
      cb: (event: HttpEvent<Object>) => {
        console.log(event);
        if (event.type === HttpEventType.UploadProgress) {
          if (event.total)
            fileObj.setProgress(Math.round((100 * event.loaded) / event.total));
        } else if (event.type === HttpEventType.Response) {
          fileObj.finishedsmooth(1000);
        }
      },
      done: (rep: FileJS) => {
        const index = this.filePendings.findIndex(
          (x) => x.file.name === rep.originalname && x.file.size === rep.size
        );
        const pending = this.filePendings[index];
        pending.path = rep.path;
        this.fileUploads.push(rep);
      },
      error: () => {
        fileObj.errorsmooth(1000);
      },
    });
  }

  removeFile(item: FileObj) {
    const dialogRef = this.dialog.open(BaseDialogComponent, {
      width: '250px',
      data: {
        title: 'Xóa file',
        content: `Bạn có thật sự muốn xóa file <b>[${item.file.name}]</b> không?`,
        data: true,
      },
    });

    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        const index = this.filePendings.findIndex((x) => x.path === item.path);
        this.filePendings.splice(index, 1);
        const indexUploads = this.fileUploads.findIndex(
          (x) => x.path === item.path
        );
        this.fileUploads.splice(indexUploads, 1);
        this.remove.emit(item);
      }
    });
  }

  handleFiles() {
    this.done.emit(this.fileUploads);
  }

  highlight(e: Event) {
    e.stopPropagation();
    e.preventDefault();
    this.isHighlight = true;
  }
  unhighlight() {
    this.isHighlight = false;
  }

  inputFileChange(event: any) {
    let files = event.target.files;
    files = [...files];
    files.forEach((file: Blob) => {
      this.uploadFile(file);
    });
  }

  onDragEnter($event: any, insertionPoint: string) {
    this.highlight($event);
  }
  onDragOver($event: any, insertionPoint: string) {
    this.highlight($event);
    $event.dataTransfer.dropEffect = 'move';
  }
  onDragLeave($event: any, insertionPoint: string) {
    this.unhighlight();
  }
  onDrop(event: any, insertionPoint: string) {
    event.stopPropagation();
    event.preventDefault();
    this.isHighlight = false;
    let files = event.dataTransfer.files;
    if (files) {
      if (this.multiple) {
        files = [...files];
        files.forEach((file: any) => {
          this.uploadFile(file);
        });
      } else {
        this.uploadFile(files[0]);
      }
    }
  }
}
