import { Injectable } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { BaseDialogComponent } from './base-dialog.component';

@Injectable({ providedIn: 'root' })
export class DialogService {
  constructor(private _dialog: MatDialog) {}
  confirm(message: string, title: string = 'Thông báo') {
    return new Promise((resolve, reject) => {
      const dialogRef = this._dialog.open(BaseDialogComponent, {
        data: {
          title: title,
          content: message,
          data: true,
          noconfirm: false,
        },
      });
      dialogRef.afterClosed().subscribe((result) => {
        if (result) resolve(result);
        // else reject();
      });
    });
  }

  open(message: string, title: string = 'Thông báo') {
    return new Promise((resolve, reject) => {
      const dialogRef = this._dialog.open(BaseDialogComponent, {
        data: {
          title: title,
          content: message,
          data: true,
          noconfirm: true,
        },
      });
      dialogRef.afterClosed().subscribe((result) => {
        resolve(result);
      });
    });
  }
}
