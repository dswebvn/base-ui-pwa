### HƯỚNG DẪN SỬ DỤNG DIALOG

---

```ts
export class AppComponent {
 constructor(private dialogService:DialogService){
 }

 dialogYesNo(){
    this.dialogService.confirm(repData.message).then((rep) => {
        if (rep) {
        alert('Chọn Đồng ý');
        } else {
        alert('Chọn Hủy bỏ');
        }
    });
 }
 
 dialogShowMes(){
    this.dialogService.open('Lỗi gì đó');
 }

}
```