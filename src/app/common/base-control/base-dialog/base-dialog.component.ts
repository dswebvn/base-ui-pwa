import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'base-dialog',
  template: `
    <div class="base-dialog">
      <div class="head d-flex justify-content-between align-items-center">
        <h3 class="title">
          <i class="fa-solid fa-xl fa-triangle-exclamation text-warning"></i>
          {{ data.title }}
        </h3>
        <button
          type="button"
          class="btn btn-sm text-right"
          (click)="onNoClick()"
        >
          <mat-icon
            aria-hidden="false"
            aria-label="Example home icon"
            fontIcon="close"
          ></mat-icon>
        </button>
      </div>

      <div class="body">
        <div mat-dialog-content [outerHTML]="data.content"></div>
      </div>
      <div class="footer">
        <div class="d-flex justify-content-end align-items-center">
          <button
            *ngIf="!data.noconfirm"
            class="btn btn-danger btn-sm d-flex align-items-center"
            mat-button
            [mat-dialog-close]="data.data"
            cdkFocusInitial
          >
            <mat-icon
              aria-hidden="false"
              aria-label="Example home icon"
              fontIcon="check"
            ></mat-icon>
            <span> Đồng ý</span>
          </button>
          <button
            class="btn btn-secondary btn-sm ms-2 d-flex align-items-center"
            mat-button
            (click)="onNoClick()"
          >
            <mat-icon
              aria-hidden="false"
              aria-label="Example home icon"
              fontIcon="close"
            ></mat-icon>
            Đóng
          </button>
        </div>
      </div>
    </div>
  `,
  styleUrls: ['./base-dialog.component.scss'],
})
export class BaseDialogComponent {
  constructor(
    public dialogRef: MatDialogRef<BaseDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData
  ) {}

  onNoClick(): void {
    this.dialogRef.close();
  }
}

export interface DialogData {
  data: string;
  title: string;
  content: string;
  noconfirm: boolean;
}
