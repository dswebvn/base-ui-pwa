import {
  Component,
  EventEmitter,
  Input,
  Output,
  SimpleChanges,
} from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'base-paging',
  template: `
    <div class="base-paging">
      <div class="summary">
        <ng-content></ng-content>
      </div>
      <div class="paging">
        <div class="dropdown-center">
          <button
            class="btn dropdown-toggle"
            type="button"
            data-bs-toggle="dropdown"
            aria-expanded="false"
          >
            {{ limit }} dòng
            <i class="fa fa-angle-down"></i>
          </button>
          <ul class="dropdown-menu">
            <li
              *ngFor="let item of listPage"
              [ngClass]="{ active: limit == item }"
              (click)="changeLimitPage(item)"
            >
              <a class="dropdown-item"
                >{{ item }} dòng <i class="fa fa-check" aria-hidden="true"></i
              ></a>
            </li>
          </ul>
        </div>
        <div class="bulet d-flex px-3">
          <button class="btn" [disabled]="page <= 1" (click)="setpage(1)">
            <i class="fa-solid fa-angles-left"></i>
          </button>
          <button
            class="btn"
            [disabled]="page <= 1"
            (click)="setpage(page - 1)"
          >
            <i class="fa-solid fa-angle-left"></i>
          </button>
          <div class="text">
            <strong>{{ from }}</strong>
            <span>đến</span>
            <strong>{{ to }}</strong>
            <span>/</span>
            <strong>{{ total }}</strong>
          </div>
          <button
            class="btn"
            [disabled]="page >= maxpage"
            (click)="setpage(page + 1)"
          >
            <i class="fa-solid fa-angle-right"></i>
          </button>
          <button
            class="btn"
            [disabled]="page >= maxpage"
            (click)="setpage(maxpage)"
          >
            <i class="fa-solid fa-angles-right"></i>
          </button>
        </div>
      </div>
    </div>
  `,
  styleUrls: ['./base-paging.component.scss'],
})
export class BasePagingComponent {
  @Input() total: number = 0;
  @Input() page: number = 0;
  @Input() keyPage: string | boolean = 'page';

  @Input() limit: number = 0;
  @Output() limitChange: EventEmitter<number> = new EventEmitter();

  @Output() change: EventEmitter<any> = new EventEmitter();
  from: number = 0;
  to: number = 0;
  maxpage: number = 0;

  listPage: number[] = [3, 10, 15, 20, 30, 50, 100, 300, 500];
  constructor(private route: ActivatedRoute) {}

  ngOnInit(): void {
    this.route.queryParams.subscribe((queries) => {
      // console.log(queries);
      if (typeof this.keyPage == 'string') {
        let page = eval(queries[this.keyPage] ? queries[this.keyPage] : 1);
        this.change.emit(page);
      }
    });
  }
  ngAfterContentInit(): void {}

  changeLimitPage(limit: number) {
    this.limit = limit;
    localStorage.setItem('page_limit', JSON.stringify(limit));
    this.limitChange.emit(limit);
    this.setpage(1);
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes && changes['total']?.currentValue) {
      this.calcPage();
    }
    if (changes && changes['page']?.currentValue) {
      this.calcPage();
      if (this.page == 1 && typeof this.keyPage == 'string')
        this.insertParam(this.keyPage, this.page + '');
    }
    if (changes && changes['limit']?.currentValue) {
      this.calcPage();
    }
  }

  calcPage() {
    this.from = this.page * this.limit - this.limit + 1;
    const totmp = this.page * this.limit;
    const ptmp = this.total / this.limit;
    this.maxpage =
      Math.round(ptmp) < ptmp ? Math.round(ptmp) + 1 : Math.round(ptmp);
    this.to = this.page < this.maxpage ? totmp : this.total;
  }

  setpage(page: number) {
    this.page = page;
    this.change.emit(this.page);
    if (typeof this.keyPage == 'string')
      this.insertParam(this.keyPage, page + '');
  }

  insertParam(key: string, value: string) {
    key = encodeURIComponent(key);
    value = encodeURIComponent(value);
    let searchParams = new URLSearchParams(window.location.search);
    searchParams.set(key, value);
    let newurl =
      window.location.protocol +
      '//' +
      window.location.host +
      window.location.pathname +
      '?' +
      searchParams.toString();
    window.history.pushState({ path: newurl }, '', newurl);
    return;
  }
}
