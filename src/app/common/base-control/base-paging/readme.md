### HƯỚNG DẪN SỬ DỤNG PAGING

> Là control dùng để phân trang

---

```html
<base-paging [total]="total" [page]="page" [(limit)]="limit" (change)="changePage($event)"> </base-paging>
```

```ts
export class AppComponent {
  public page: number = 1;
  public limit: number = 10;
  public search: string = "";
  public total: number = 0;

  constructor() {}

  changePage(page: number) {
    this.page = page;
    // this.loadData();
  }
}
```

| Input   | mô tả                                   | kiểu dữ liệu | ví dụ   |
| ------- | --------------------------------------- | ------------ | ------- |
| total   | Tổng số dòng                            | number       | 1000    |
| page    | trang hiện tại                          | number       | 1       |
| limit   | số dòng trên 1 trang                    | number       | 10      |
| keyPage | Từ khóa page trên URL, mặc định là page | string       | boolean |

| Sự kiện | mô tả                             |
| ------- | --------------------------------- |
| onInit  | Kích hoạt khi block khởi tạo xong |

| Hàm    | mô tả        |
| ------ | ------------ |
| load   | bật loading  |
| unload | hủy loadding |
