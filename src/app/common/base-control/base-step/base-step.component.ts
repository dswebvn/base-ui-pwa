import { Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'base-step',
  template: `
    <div class="stepper">
      <div class="stepper-bar">
        <div
          class="stepper-item"
          (click)="changeStep(item.step)"
          *ngFor="let item of list"
          [ngClass]="{ active: step == item.step, completed: step > item.step }"
        >
          <div class="step-counter">{{ item.step }}</div>
          <div class="label">{{ item.label }}</div>
        </div>
      </div>
    </div>
  `,
  styleUrls: ['./base-step.component.scss'],
})
export class BaseStepComponent {
  @Input() step: number = 0;
  @Input() onlyStep: string | boolean = false;
  @Output() stepChange: EventEmitter<number> = new EventEmitter();

  @Input() list: Step[] = [];
  constructor() {}

  ngOnInit(): void {}

  changeStep(step: number) {
    if (this.onlyStep) {
      if (this.step > step) {
        this.step = step;
        this.stepChange.emit(step);
      }
    } else {
      this.step = step;
      this.stepChange.emit(step);
    }
  }
}

export class Step {
  step: number = 0;
  label: string = '';
}
