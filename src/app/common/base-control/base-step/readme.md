### HƯỚNG DẪN SỬ DỤNG STEP

> Control Từng bước (timeline), cho phép click vào step để nhảy bước hoặc nhảy bước chủ động

---

```html
<base-step
  class="w-100"
  [(step)]="step"
  onlyStep="true"
  (stepChange)="stepChange($event)"
  [list]="[
        { step: 1, label: 'Chọn tệp nguồn' },
        { step: 2, label: 'Ghép dữ liệu' },
        { step: 3, label: 'Xem trước' },
        { step: 4, label: 'Nhập khẩu' }
      ]"
></base-step>
```

```ts
export class AppComponent {
  step: number = 1; // thay đổi để nhảy

  constructor(private printService: PrintService) {}

  next() {
    this.step = this.step + 1;
  }
}
```

| Input    | mô tả                                 | kiểu dữ liệu | ví dụ      |
| -------- | ------------------------------------- | ------------ | ---------- |
| step     | bước nhảy hiện tại                    | number       |            |
| onlyStep | Kích hoạt nếu muốn khộng chọn từ step | boolean      | 'abcprint' |
| list     | Danh sách step { step, label}         | object       | 'In ngay'  |

| Sự kiện    | mô tả                       |
| ---------- | --------------------------- |
| stepChange | Kích hoạt khi Thay đổi step |
