### HƯỚNG DẪN SỬ DỤNG PRINT
> Control In ấn, có thể gọi in bằng service hoặc kích hoạt in bằng button mặc định
---

```html
<base-print pid="abcprint" label="in đơn" hidden>
    Nội dung cần in
</base-print>
```
```ts
export class AppComponent {
    constructor(private printService:PrintService){
    }

    show(){
        this.printService.print('abcprint');
    }
}
```

| Input      | mô tả                                         | kiểu dữ liệu   | ví dụ        |
| ---------- | --------------------------------------------- | -------------- | ------------ |
| printTitle  | Tiêu đề của float                            | string         |              |
| pid         | ID định danh dùng để gọi                     | string         | 'abcprint'   |
| label       | Nhãn của button in                           | string         | 'In ngay'    |
| debug       | Khi debug thì sẽ show giao diện ko in        | boolean        | true         |
| nobutton    | Không hiển thị nút in                        | boolean        | true         |
| printDelay  | Một số dữ liệu lớn cần thời gian render lớn  | number         | 1000         |
| hidden      | Khi không muốn hiển thị vùng in ra view      | boolean        |              |

| Hàm    | mô tả          |
| ------ | -------------- |
| print  | Kích hoạt In   |