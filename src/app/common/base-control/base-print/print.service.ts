import { Injectable } from '@angular/core';
import { BasePrintComponent } from './base-print.component';

@Injectable({ providedIn: 'root' })
export class PrintService {
  private modals: BasePrintComponent[] = [];
  add(modal: any) {
    // add modal to array of active modals
    this.modals.push(modal);
  }

  remove(id: string) {
    // remove modal from array of active modals
    this.modals = this.modals.filter((x) => x.pid !== id);
  }

  print(id: string) {
    // open modal specified by id
    const modal = this.modals.find((x) => x.pid === id);
    if (modal) modal.print();
  }
}
