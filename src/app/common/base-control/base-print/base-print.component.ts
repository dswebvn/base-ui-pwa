import { Component, ElementRef, Input } from '@angular/core';
import { PrintService } from './print.service';

@Component({
  selector: 'base-print',
  template: `
    <div class="print">
      <button *ngIf="!nobutton" (click)="print()" class="btn btn-info btn-sm"><i class="fa-solid fa-print me-2"></i>{{ label }}</button>
      <div class="print-body" [id]="pid">
        <ng-content></ng-content>
      </div>
    </div>

  `,
  styleUrls: ['./base-print.component.scss']
})
export class BasePrintComponent {
  @Input() pid: string = 'print_' + Math.random().toString(36).substring(2);
  @Input() printTitle: string = '';
  @Input() label: string = '';
  @Input() debug: boolean | string = false;
  @Input() nobutton: boolean | string = false;
  /**
   * A delay in milliseconds to force the print dialog to wait before opened. Default: 0
   */
  @Input() printDelay: number = 0;

  // constructor() {}

  constructor(
    private printService: PrintService,
    private _element: ElementRef
  ) {}

  ngOnInit(): void {
    this.printService.add(this);
  }

  ngOnDestroy(): void {
    this.printService.remove(this.pid);
    this._element.nativeElement.remove();
  }

  public print(): void {
    let printContents, popupWin;
    const baseTag = this.getElementTag('base');
    printContents = this.getHtmlContents();

    let loadding = `
       <style>
          #content-print{
                visibility: hidden;
          }
          #content-print-label{
              padding:30px 50px;
              visibility: visible;
          }
          @media print {
            #content-print {
              visibility: visible;
            }
            #content-print-label{
                display:none;
            }
          }
      </style>
      <div id="content-print-label">Bắt đầu in ...</div>
    `;
    if (this.debug) loadding = '';

    popupWin = window.open('', '_blank', 'top=0,left=0,height=auto,width=auto');
    if (popupWin) {
      popupWin.document.open();
      popupWin.document.write(`
        <html>
          <head>
            <title>${this.printTitle}</title>
            <link rel="stylesheet" href="/assets/reset.css">
            <link rel="stylesheet" href="/bootstrap/dist/css/bootstrap.min.css">
            <link rel="stylesheet" href="/assets/print.css">
            ${baseTag}
          </head>
          <body>
            ${loadding}
            <div id="content-print">
              ${printContents}
            </div>
            <script defer>
              function triggerPrint(event) {
                window.removeEventListener('load', triggerPrint, false);
                setTimeout(function() {
                  closeWindow(window.print());
                }, ${this.printDelay});
              }
              function closeWindow(){
                window.close();
              }
               ${
                 !this.debug
                   ? "window.addEventListener('load', triggerPrint, false);"
                   : ''
               }
              
            </script>
          </body>
        </html>`);
      popupWin.document.close();
    }
  }

  private getHtmlContents() {
    let printContents = document.getElementById(this.pid);
    if (printContents) {
      let innards = printContents.getElementsByTagName('input');
      this.getFormData(innards);

      let txt = printContents.getElementsByTagName('textarea');
      this.getFormData(txt);

      return printContents.innerHTML;
    }
    return '';
  }

  /**
   * @returns string which contains the link tags containing the css which will
   * be injected later within <head></head> tag.
   *
   */
  private getElementTag(tag: keyof HTMLElementTagNameMap): string {
    const html: string[] = [];
    const elements = document.getElementsByTagName(tag);
    for (let index = 0; index < elements.length; index++) {
      html.push(elements[index].outerHTML);
    }
    return html.join('\r\n');
  }

  /**
   *
   * @param data the html element collection to save defaults to
   *
   */
  private getFormData(data: any) {
    for (var i = 0; i < data.length; i++) {
      data[i].defaultValue = data[i].value;
      if (data[i].checked) {
        data[i].defaultChecked = true;
      }
    }
  }
}
