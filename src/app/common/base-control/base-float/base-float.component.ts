import { Component,
  ContentChild,
  ElementRef,
  Input,
  OnInit,
  TemplateRef, } from '@angular/core';
import { FloatService } from './float.service';

@Component({
  selector: 'base-float',
  template: `
    <div class="base-float" [ngClass]="{ show: isShow }">
      <div class="overlay"></div>
      <div class="dialog">
        <div class="head">
          <ng-container [ngTemplateOutlet]="header ? header : defaultHeader" [ngTemplateOutletContext]="{ data: this }"></ng-container>
          <ng-template #defaultHeader>
            <h3 class="title">{{ title }}</h3>
            <button class="btn" (click)="isShow = false"><i class="fa-solid fa-xmark"></i></button>
          </ng-template>
        </div>
        <div class="body scrollbar style-3">
          <ng-content></ng-content>
        </div>
        <div class="footer">
          <ng-container [ngTemplateOutlet]="footer"></ng-container>
          <button type="button" class="btn btn-secondary" (click)="isShow = false">
            <i class="fa-solid fa-xmark me-2"></i>
            Đóng
          </button>
        </div>
      </div>
    </div>
  `,
  styleUrls: ['./base-float.component.scss']
})
export class BaseFloatComponent {
  @ContentChild('header') header: TemplateRef<{}> | any;
  @ContentChild('footer') footer: TemplateRef<{}> | any;

  public isShow: boolean | string = false;
  private _element: any;
  @Input() id: string | any =
    'float_' + Math.random().toString(36).substring(2);
  @Input() title: string = 'Tiêu đề';
  constructor(private floatService: FloatService, el: ElementRef) {
    this._element = el.nativeElement;
  }

  ngOnInit(): void {
    this.floatService.add(this);
  }

  ngOnDestroy(): void {
    this.floatService.remove(this.id);
    this._element.remove();
  }

  open() {
    this.isShow = true;
  }

  close() {
    this.isShow = false;
  }
}

