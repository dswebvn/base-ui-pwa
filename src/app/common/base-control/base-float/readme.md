### HƯỚNG DẪN SỬ DỤNG FLOAT

---

```html
<base-float id="demo_float" #float>
    <ng-template #header>
        Nội dung header
    </ng-template>
    <ng-template #footer>
        <button type="submit" (click)="save(frm)" class="btn btn-success me-2" [disabled]="loading">
            <i class="fa fa-spinner fa-spin me-2" *ngIf="loading"></i>
            <i class="fa fa-save px-1 me-2"></i>Lưu
        </button>
    </ng-template>
</base-float>
```
```ts
export class AppComponent {
    constructor(private floatService:FloatService){
    }

    show(){
        this.floatService.open('demo_float');
    }
}
```

| Input      | mô tả                                    | kiểu dữ liệu   | ví dụ        |
| ---------- | ---------------------------------------- | -------------- | ------------ |
| title      | Tiêu đề của float                        | string         |              |
| id         | ID định danh dùng để gọi                 | string         | 'demo_float' |

| Hàm    | mô tả        |
| ------ | ------------ |
| open   | bật float    |
| close  | đóng float   |