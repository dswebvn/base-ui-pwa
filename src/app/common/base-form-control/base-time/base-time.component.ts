import { Component, Input, forwardRef } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';

@Component({
  selector: 'base-time',
  template: `
    <div class="form-group" [class]="class" [ngClass]="{ 'form-row': row }">
      <label *ngIf="label">
        {{ label }}
        <span class="text-danger ms-1" *ngIf="required">*</span>
      </label>
      <input
        type="text"
        min="1"
        [required]="required"
        hidden
        #text="ngModel"
        [(ngModel)]="value"
      />
      <div class="time-control">
        <input
          class="form-control"
          min="0"
          [(ngModel)]="min"
          type="number"
          [ngClass]="{ 'ng-invalid': text.errors, 'ng-valid': !text.errors }"
        />
        <span class="mx-2">Phút</span>
        <input
          class="form-control"
          type="number"
          #zsec="ngModel"
          [(ngModel)]="sec"
          [ngClass]="{ 'ng-invalid': text.errors, 'ng-valid': !text.errors }"
        />
        <span min="0" class="ms-2">giây</span>
        <input-validate
          [submit]="submit || zsec.dirty"
          [parent]="text"
          [name-control]="label"
        ></input-validate>
      </div>
    </div>
  `,
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => BaseTimeComponent),
      multi: true,
    },
  ],
  styleUrls: ['./base-time.component.scss'],
})
export class BaseTimeComponent implements ControlValueAccessor {
  private _min: number = 0;
  private _sec: number = 0;
  private _value: any = 0;

  @Input() submit: string | boolean = false;
  @Input() tab: string | number = '1';
  @Input() required: boolean = false;
  @Input() label: string = '';
  @Input() row: string | boolean = '';
  @Input() class: string = '';
  @Input() placeholder: string = '';

  constructor() {}

  _onChange: any = () => {};
  _onTouched: any = () => {};
  disabled = false;

  registerOnChange(fn: any): void {
    this._onChange = fn;
  }
  registerOnTouched(fn: any): void {
    this._onTouched = fn;
  }
  setDisabledState?(isDisabled: boolean): void {
    this.disabled = isDisabled;
  }
  public get value() {
    return this._value;
  }

  public get min() {
    return this._min;
  }
  public get sec() {
    return this._sec;
  }

  set value(val: number) {}

  set min(val: number) {
    if (val == null) {
      this._min = 0;
    } else {
      this._min = val;
    }

    this.writeValue({
      min: this._min,
      sec: this._sec,
    });
  }

  set sec(val: number) {
    if (val == null) {
      this._sec = 0;
    } else {
      this._sec = val;
    }

    this.writeValue({
      min: this._min,
      sec: this._sec,
    });
  }
  // This will will write the value to the view if the the value changes occur on the model programmatically
  writeValue(value: any) {
    this._min = value?.min;
    this._sec = value?.sec;
    const cvalue = this._min * 60 + this._sec;
    if (cvalue > 0) {
      this._value = cvalue;
    } else {
      this._value = null;
    }
    this._onChange(value);
    this._onTouched(value);
  }
}
