import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BaseInputComponent } from './base-input/base-input.component';
import { BaseSelectComponent } from './base-select/base-select.component';
import { BaseTimeComponent } from './base-time/base-time.component';
import { InputValidateComponent } from './base-input/input-validate.component';
import { FormsModule } from '@angular/forms';
import { BaseImportComponent } from './base-import/base-import.component';
import { BaseControlModule } from '@base/control/base-control.module';

@NgModule({
  declarations: [
    InputValidateComponent,
    BaseSelectComponent,
    BaseInputComponent,
    BaseTimeComponent,
    BaseImportComponent,
  ],
  imports: [CommonModule, FormsModule, BaseControlModule],
  exports: [
    BaseSelectComponent,
    BaseInputComponent,
    BaseTimeComponent,
    BaseImportComponent,
  ],
})
export class BaseFormControlModule {}
