import { Component, EventEmitter, Input, Output } from '@angular/core';
import StringHelper from '@base/helper/string.helper';
import { ExcelMap } from '@base/models/excel-map.model';
import { ObjectLiteral } from '@base/models/object.model';
import * as XLSX from 'xlsx';
type AOA = any[][];

@Component({
  selector: 'base-import',
  templateUrl: './base-import.component.html',
  styleUrls: ['./base-import.component.scss'],
})
export class BaseImportComponent {
  public error: string | undefined | null;
  public step: number = 1;
  public file: File | undefined;
  public isHighlight: boolean = false;
  public autoId: string = 'upload_' + Math.random().toString(36).substring(2);
  public typeImport: number = 1;
  public header: string[] = [];
  public tabMapIndex: number = 0; // màn hình xem trước : chọn tab nào (0 : tất cả, 1 : đã ghép, 2: chưa ghép)

  public disableNext: boolean = false;

  _headerObject: ObjectLiteral[] = [];
  _data: any[] = [];
  _dataRs: any[] = [];
  constructor() {}

  @Input() title: string = 'Nhập khẩu';
  @Input() exam: string = '/assets/import';
  @Input() objectMapper: ExcelMap[] = [];
  @Input() objectMapperView: ExcelMap[] = [];
  @Output() import: EventEmitter<any> = new EventEmitter();

  // mapper: ObjectLiteral = {};

  ngOnInit(): void {}

  handleFile(file: File) {
    let reader = new FileReader();
    var self = this;
    // reader.readAsDataURL(file);
    reader.readAsBinaryString(file);
    reader.onloadend = function () {
      self.file = file;
      try {
        self.excelReader(reader.result);
      } catch (error: any) {
        self.error = error.message;
        self.removeFile();
      }
      //self.doUpload(file, reader.result);
    };
  }

  chooseTabMap(index: number) {
    this.tabMapIndex = index;

    if (index == 0) {
      this.objectMapperView = this.objectMapper;
    } else if (index == 1) {
      this.objectMapperView = [];
      this.objectMapper.forEach((element) => {
        if (element.indexMap != undefined && element.indexMap >= 0) {
          this.objectMapperView.push(element);
        }
      });
    } else {
      this.objectMapperView = [];
      this.objectMapper.forEach((element) => {
        if (element.indexMap == undefined || element.indexMap < 0) {
          this.objectMapperView.push(element);
        }
      });
    }
  }

  excelReader(bstr: any) {
    const wb: XLSX.WorkBook = XLSX.read(bstr, { type: 'binary' });
    //read first sheetthis\._header
    const wsname: string = wb.SheetNames[0];
    const ws: XLSX.WorkSheet = wb.Sheets[wsname];
    const data = <AOA>XLSX.utils.sheet_to_json(ws, { header: 1 });
    this._headerObject = [];

    if (data && data.length > 0) {
      this.header = data[0];
      for (let i = 0; i < this.header.length; i++) {
        const element = this.header[i];
        this._headerObject.push({
          id: i,
          label: element,
        });
      }

      this._data = data.slice(0, data.length);
    } else {
      throw { message: 'Đọc file lỗi' };
    }
  }

  //Khởi tạo map key giữa nguồn excel và đối tượng cần
  excelExtractMapper() {
    this.objectMapper.forEach((map) => {
      map.indexMap = this.getIndexHeaderFromKey(map);
    });
    this.chooseTabMap(this.tabMapIndex);
  }

  getIndexHeaderFromKey(key: ExcelMap) {
    for (let i = 0; i < this.header.length; i++) {
      const element = this.header[i];
      const mk = StringHelper.toASSII(element.toLocaleLowerCase());
      if (
        mk == StringHelper.toASSII(key.name.toLocaleLowerCase()) ||
        mk == StringHelper.toASSII(key.param.toLocaleLowerCase())
      )
        return i;
    }
    return -1;
  }

  excelMapper() {
    this._dataRs = [];
    this.disableNext = true;
    for (let i = 1; i < this._data.length; i++) {
      const row = this._data[i];
      let tmp: ObjectLiteral = {};
      this.objectMapper.forEach((map) => {
        try {
          if (map.indexMap != undefined && map.indexMap >= 0)
            tmp[map.param] = row[map.indexMap];
          else tmp[map.param] = '';
        } catch (error) {
          tmp[map.param] = '';
        }
      });
      this._dataRs.push(tmp);
    }
    console.log(this._dataRs);
    this.disableNext = false;
  }

  resetControl() {
    this.file = undefined;
    this._dataRs = [];
    this._data = [];
    this._headerObject = [];
    this.header = [];
    this.step = 1;
    this.tabMapIndex = 0;
  }

  doImport() {
    this.import.emit(this._dataRs);
    this.resetControl();
  }

  next() {
    this.step += 1;
    switch (this.step) {
      case 1:
        this.excelExtractMapper();
        break;
      case 2:
        this.excelExtractMapper();
        break;
      case 3:
        this.excelMapper();
        break;

      default:
        break;
    }
  }
  prev() {
    this.step -= 1;
    if (this.step <= 1) this.step = 1;
  }

  removeFile() {
    this.file = undefined;
  }
  inputFileChange(event: any) {
    let files = event.target.files;
    files = [...files];
    files.forEach((file: File) => {
      this.handleFile(file);
    });
  }

  highlight(e: Event) {
    e.stopPropagation();
    e.preventDefault();
    this.isHighlight = true;
  }
  unhighlight() {
    this.isHighlight = false;
  }
  onDragEnter($event: any, insertionPoint: string) {
    this.highlight($event);
  }
  onDragOver($event: any, insertionPoint: string) {
    this.highlight($event);
    $event.dataTransfer.dropEffect = 'move';
  }
  onDragLeave($event: any, insertionPoint: string) {
    this.unhighlight();
  }
  onDrop(event: any, insertionPoint: string) {
    event.stopPropagation();
    event.preventDefault();
    this.isHighlight = false;
    let files = event.dataTransfer.files;
    if (files) {
      files = [...files];
      if (files.length > 0) {
        this.handleFile(files[0]);
      }
    }
  }
}
