### HƯỚNG DẪN SỬ DỤNG BASE-INPUT

---

**Cơ Bản**

```html
<base-input [required]="true" name="fullName" label="Họ và tên" [(ngModel)]="user.fullName" placeholder="Họ và tên" class="mb-3"></base-input>
```

| Input  | mô tả                                                                                                             |
| ------ | ----------------------------------------------------------------------------------------------------------------- |
| list   | Danh sách option cần chọn                                                                                         |
| label  | gắng nhãn cho button open                                                                                         |
| config | Cấu hình select để chỉ định cột giá trị và cột hiển thị mặc định sẽ là valueMember: 'value',displayMember: 'text' |
| key    | sử dụng nếu muốn lấy 1 giá trị, nếu không gán key thì trả về nguyên object                                        |
| submit | trạng thái submit của form (dùng trong trường hợp validate)                                                       |
| row    | **true** nếu muốn label trên 1 dòng                                                                               |

| Sự kiện | mô tả                           |
| ------- | ------------------------------- |
| ngModel | Databinding 2 chiều cho control |
