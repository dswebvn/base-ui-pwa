import { Component, forwardRef, Input } from '@angular/core';
import {
  ControlValueAccessor,
  NgControl,
  NG_VALUE_ACCESSOR,
} from '@angular/forms';

@Component({
  selector: 'base-input',
  templateUrl: './base-input.component.html',
  styleUrls: ['./base-input.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => BaseInputComponent),
      multi: true,
    },
  ],
})
export class BaseInputComponent implements ControlValueAccessor {
  private _value: string = '';
  @Input() submit: string | boolean = false;
  @Input() tab: string | number = '1';
  @Input() required: boolean = false;
  // @Input() disabled: boolean = false;
  @Input() label: string = '';
  @Input() row: string | boolean = '';
  @Input() class: string = '';
  @Input() placeholder: string = '';

  @Input() id: string = 'basei_' + Math.random().toString(36).substring(2);

  @Input() type:
    | 'text'
    | 'checkbox'
    | 'search'
    | 'number'
    | 'email'
    | 'password'
    | 'date'
    | 'datetime-local' = 'text';

  constructor() {}

  _onChange: any = () => {};
  _onTouched: any = () => {};
  disabled = false;

  registerOnChange(fn: any): void {
    this._onChange = fn;
  }
  registerOnTouched(fn: any): void {
    this._onTouched = fn;
  }
  setDisabledState?(isDisabled: boolean): void {
    this.disabled = isDisabled;
  }
  public get value() {
    return this._value;
  }
  // sets the value used by the ngModel of the element
  set value(val: string) {
    this._value = val;
    this._onChange(val);
    this._onTouched(val);
  }

  // This will will write the value to the view if the the value changes occur on the model programmatically
  writeValue(value: any) {
    this._value = value;
  }

  clearValue() {
    this._value = '';
    this._onChange('');
    this._onTouched('');
  }
}
