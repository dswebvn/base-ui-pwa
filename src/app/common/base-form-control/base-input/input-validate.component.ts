import { Component, Input } from '@angular/core';
import { NgControl, NgModel } from '@angular/forms';

@Component({
  selector: 'input-validate',
  template: `
    <div class="alert alert-danger" role="alert" *ngIf="message">
      <div class="arrow up"></div>
      <span>{{ message }}</span>
    </div>
  `,
  styles: [
    `
      .alert {
        // display: none;
        padding: 0 10px;
        position: absolute;
        right: 0px;
        border: 1px solid #dc3545;
        border-top: none;
        border-radius: 0;
        z-index: 9;
        .arrow {
          border: solid #dc3545;
          border-width: 0 1px 1px 0;
          display: inline-block;
          padding: 3px;
          position: absolute;
          top: -5px;
          right: 12px;
          &.right {
            transform: rotate(-45deg);
            -webkit-transform: rotate(-45deg);
          }

          &.left {
            transform: rotate(135deg);
            -webkit-transform: rotate(135deg);
          }

          &.up {
            transform: rotate(-135deg);
            -webkit-transform: rotate(-135deg);
          }

          &.down {
            transform: rotate(45deg);
            -webkit-transform: rotate(45deg);
          }
        }
      }
    `,
  ],
})
export class InputValidateComponent {
  @Input() submit: string | boolean | null = false;
  @Input('control') control: NgControl | any;
  @Input('parent') parent: NgModel | any;
  @Input('name-control') controlName: any;
  constructor() {}

  ngOnInit(): void {}

  get message() {
    if (this.parent)
      for (const err in this.parent.errors) {
        if (this.parent.dirty || this.submit) {
          return this.getErrorMessage(err, this.parent.errors[err]);
        }
      }

    // if(this.control)
    // for (const err in this.control.errors) {
    //   if(this.control.dirty || this.parent.formDirective && this.parent.formDirective?.submitted) {
    //     return this.getErrorMessage(err, this.control.errors[err]);
    //   }
    // }
  }

  getErrorMessage(error: string, value: any) {
    let messages: any = {
      required: `${this.controlName} buộc phải nhập`,
      minlength: `${this.controlName} cần tối thiểu ${value.requiredLength} ký tự .`,
      maxlength: `${this.controlName} tối đa có ${value.requiredLength} ký tự .`,
      pattern: `${this.controlName} chưa đúng định dạng`,
      mustMatch: `${this.controlName} phải khớp`,
    };
    return messages[error];
  }
}
