### HƯỚNG DẪN SỬ DỤNG BASE-SELECT

---

**Cơ Bản**

```html
<base-select
  [list]="[
      { text: 'Hoàn thành', value: true },
      { text: 'Thất bại', value: 0 }
    ]"
  label="Trạng thái"
  [(ngModel)]="call.isFinish"
  name="isFinish"
  row="true"
></base-select>
```

**Nâng cao**

```html
<base-select [ajax]="ajaxUser" label="Người thực hiện" [config]="{ displayMember: 'fullName', valueMember: 'userId' }" [(ngModel)]="call.excutedBy" key="userId" [submit]="frm.submitted" [required]="true" name="excutedBy" row="true"> </base-select>
```

| Input  | mô tả                                                                                                             |
| ------ | ----------------------------------------------------------------------------------------------------------------- |
| list   | Danh sách option cần chọn                                                                                         |
| label  | gắng nhãn cho button open                                                                                         |
| config | Cấu hình select để chỉ định cột giá trị và cột hiển thị mặc định sẽ là valueMember: 'value',displayMember: 'text' |
| key    | sử dụng nếu muốn lấy 1 giá trị, nếu không gán key thì trả về nguyên object                                        |
| submit | trạng thái submit của form (dùng trong trường hợp validate)                                                       |
| row    | **true** nếu muốn label trên 1 dòng                                                                               |
| ajax   | khi muốn load select từ api                                                                                       |

| Sự kiện | mô tả                           |
| ------- | ------------------------------- |
| ngModel | Databinding 2 chiều cho control |
