import {
  Component,
  EventEmitter,
  Input,
  Output,
  SimpleChanges,
  TemplateRef,
  forwardRef,
} from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import StringHelper from '@base/helper/string.helper';

@Component({
  selector: 'base-select',
  templateUrl: './base-select.component.html',
  styleUrls: ['./base-select.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => BaseSelectComponent),
      multi: true,
    },
  ],
})
export class BaseSelectComponent implements ControlValueAccessor {
  private _value: any = null;
  @Input() submit: string | boolean = false;
  @Input() tab: string | number = '1';
  @Input() required: boolean = false;
  @Input() label: string = '';
  @Input('default') defaultText: string = '--Không chọn--';
  @Input() row: string | boolean = '';
  @Input() nosearch: string | boolean = '';
  @Input() nodefault: string | boolean = '';
  @Input() defaultValue: any | boolean = false;
  @Input() mutiple: string | boolean = false;
  @Input() class: string = '';

  @Input() placeholder: string = '';
  @Input() config: { valueMember: string; displayMember: string } = {
    valueMember: 'value',
    displayMember: 'text',
  };
  @Input('defaultOption') customOption: TemplateRef<any> | undefined;
  @Output() change = new EventEmitter<any>();
  @Input() key: string | null = null;
  @Input() list: any[] = [];
  @Input() ajax: any; //= () => {};
  public keyword: string = '';
  public isLoading: boolean = false;
  public listVib: any[] = [];
  public text: string = '';
  constructor() {}

  _onChange: any = () => {};
  _onTouched: any = () => {};
  disabled = false;
  dirty = false;
  firstLoad: boolean = false;

  registerOnChange(fn: any): void {
    this._onChange = fn;
  }
  registerOnTouched(fn: any): void {
    this._onTouched = fn;
  }
  setDisabledState?(isDisabled: boolean): void {
    this.disabled = isDisabled;
  }
  public get value() {
    return this._value;
  }
  // sets the value used by the ngModel of the element
  set value(val: any) {
    this._value = val;
    this._onChange(val);
    this._onTouched(val);
    this.change.emit(val);

    if (this.mutiple) {
      this.mutipleChange();
    } else {
      this.findTextValue();
    }
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes && changes['list']?.currentValue) {
      console.log('RELOAD', this.label, this.list);
      this.loadData();
    }
  }

  timerSearch: any;
  doSearch() {
    clearTimeout(this.timerSearch);
    this.timerSearch = setTimeout(() => {
      this.loadData();
    }, 250);
  }

  loadData() {
    this.listVib = [];
    if (this.ajax) {
      this.isLoading = true;
      this.ajax(
        this.keyword,
        (data = []) => {
          this.list = data;
          this.listVib = data;

          if (!this.firstLoad) {
            this.findTextValue();
            this.firstLoad = true;
          }
          this.setCheckedInit();
        },
        () => {
          this.isLoading = false;
        }
      );
    } else {
      if (!this.keyword || this.keyword.length == 0) this.listVib = this.list;
      else {
        this.listVib = this.list.filter((element) => {
          let m = StringHelper.toASSIISearch(
            element[this.config.displayMember].toLowerCase()
          );
          let s = StringHelper.toASSIISearch(this.keyword.toLowerCase());
          return m.includes(s);
        });
        this.setCheckedInit();
      }

      this.findTextValue();
    }
  }

  setCheckedInit() {
    if (!this.mutiple) return;
    if (!this._value) {
      this._value = [];
    }
    this.text = '';

    for (let i = 0; i < this.listVib.length; i++) {
      const element = this.listVib[i];
      element.checked = false;
      if (this.key) {
        if (this._value.includes(element[this.key])) {
          element.checked = true;
          this.text = this.text + ', ' + element[this.config.displayMember];
        }
      } else {
        for (let j = 0; j < this._value.length; j++) {
          const val = this._value[j];
          if (
            element[this.config.valueMember] === val[this.config.valueMember]
          ) {
            element.checked = true;
            this.text = this.text + ', ' + element[this.config.displayMember];
            continue;
          }
        }
      }
    }
    if (this._value.length > 3) {
      this.text = this._value.length + ' mục đã chọn';
    } else {
      this.text = StringHelper.trimChar(this.text.trim(), ',');
    }
  }
  mutipleChange() {
    let vs = [];
    this.text = '';
    if (this.key) {
      for (let i = 0; i < this.listVib.length; i++) {
        const element = this.listVib[i];
        if (element.checked) {
          vs.push(element[this.key]);
          this.text = this.text + ', ' + element[this.config.displayMember];
        }
      }
    } else {
      for (let i = 0; i < this.listVib.length; i++) {
        const element = this.listVib[i];
        if (element.checked) {
          vs.push(element);
          this.text = this.text + ', ' + element[this.config.displayMember];
        }
      }
    }

    this._value = vs;
    if (this._value.length > 3) {
      this.text = this._value.length + ' mục đã chọn';
    } else {
      this.text = StringHelper.trimChar(this.text.trim(), ',');
    }
    this._onChange(vs);
    this._onTouched(vs);
    this.change.emit(vs);
  }

  choose(item: any, e: any) {
    if (this.mutiple) {
      item.checked = !item.checked;
      this.mutipleChange();
      e.stopPropagation();
    } else {
      this.dirty = true;
      if (this.key && item) this._value = item[this.key];
      else this._value = item;

      if (this._value == null && this.defaultValue) {
        this._value = this.defaultValue;
      }
      this._onChange(this._value);
      this._onTouched(this._value);
      this.change.emit(this._value);
      this.findTextValue();
    }
  }

  findTextValue() {
    console.log('listVib', this.label, this.listVib);
    if (
      !this.key ||
      this._value == null ||
      this._value == undefined ||
      this._value == -1
    )
      return;

    for (let i = 0; i < this.listVib.length; i++) {
      const element = this.listVib[i];
      if (element[this.key] == this.value) {
        this.text = element[this.config.displayMember];
        console.log('text', this.text);
        return;
      }
    }
  }

  writeValue(value: any) {
    if (value == undefined) value = null;
    this._value = value;
    if (!this.firstLoad && this.ajax && value && value != '') {
      this.loadData();
    } else {
      if (this.mutiple) {
        if (typeof this._value == 'string') {
          try {
            this._value = JSON.parse(this._value);
          } catch (error) {
            this._value = null;
          }
        }
        this.setCheckedInit();
      } else {
        this.findTextValue();
      }
    }
  }
}
