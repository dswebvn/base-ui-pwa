import { UsrPer } from '@app/models/sec/usr-per.model';
import { AuthService } from '@app/share/services/auth.service';
import { ServiceLocator } from './locator.service';

export class Common {
  static _instance: Common | null = null;
  private _permissions: UsrPer[] = [];
  private authService: AuthService;
  constructor() {
    this.authService = ServiceLocator.injector.get(AuthService);
    this._permissions = this.authService.permissions();
  }

  public static getInstance() {
    if (!this._instance) {
      this._instance = new Common();
    }
    this._instance._permissions = this._instance.authService.permissions();
    return this._instance;
  }

  public permissions() {
    return this._permissions;
  }

  public checkPermision(
    page: string = '',
    act: string = '',
    functions: string = ''
  ) {
    let pagePermission = this._permissions.filter(
      (x) => x.controllerName.toLowerCase() == page.toLowerCase()
    );
    if (act)
      pagePermission = pagePermission.filter(
        (x) => x.actionName.toLowerCase() == act.toLowerCase()
      );

    let per = pagePermission.findIndex(
      (x) => x.functionId.toLowerCase() == functions.toLowerCase()
    );
    if (per >= 0) return true;
    return false;
  }
}

/**
 * Checks if the user has permission to perform a certain action on a page.
 * @param page - The name of the page to check permission for.
 * @param act - The action to check permission for.
 * @param functions - Optional. A comma-separated list of functions to check permission for.
 * If not provided, the `act` parameter will be used as the only function to check permission for.
 * @returns A boolean indicating whether the user has permission to perform the specified action(s) on the page.
 */
export function checkPermission(
  page: string,
  act: string,
  functions: string = ''
) {
  if (functions.length == 0) {
    functions = act;
    act = 'INDEX';
  }
  return Common.getInstance().checkPermision(page, act, functions);
}

export enum FUNC {
  VIEW = 'VIEW',
  ADD = 'ADD',
  DEL = 'DEL',
  EDIT = 'EDIT',
  PRINT = 'PRINT',
  PREVIEW = 'PREVIEW',
  POST = 'POST',
  UNPOST = 'UNPOST',
  EXPORT = 'EXPORT',
  IMPORT = 'IMPORT',
  APPROVE = 'APPROVE',
  FILTER = 'FILTER',
  VALUE = 'VALUE',
  PROMOTION = 'PROMOTION',
  REFRESH_COST = 'REFRESH_COST',
  CLOSING = 'CLOSING',
  ADDSERVICE = 'ADDSERVICE',
  DATE = 'DATE',
}
