export interface ExcelMap {
  param: string;
  name: string;
  indexMap?: number;
  desc?: string;
}
