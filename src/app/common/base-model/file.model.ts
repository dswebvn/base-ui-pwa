export class FileJS {
  destination: string = ''; // 'uploads/attach';
  encoding: string = ''; //'7bit';
  fieldname: string = ''; // 'file';
  filename: string = ''; // 'bg-home-1_a167f633eb88009c2dbbcc5602bac902.jpg';
  mimetype: string = ''; // 'image/jpeg';
  originalname: string = ''; // 'bg_home_1.jpg';
  path: string = ''; //'\\attach\\bg-home-1_a167f633eb88009c2dbbcc5602bac902.jpg';
  size: number = 0; // 242986;
}
