export enum TypeFile {
  FOLDER = 'FOLODER',
  FILE = 'FILE',
}

export class FileObj {
  file: File;
  status: boolean = false;
  progress: number = 0;
  preview: any;
  path: string = '';
  isError: boolean = false;

  timerProgress: any;

  constructor(item: File) {
    this.file = item;
  }

  public checkType() {
    let arr = [
      'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
      'application/pdf',
      'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
      'application/x-zip-compressed',
    ];
    if (arr.indexOf(this.file.type)) {
      return true;
    }
    return false;
  }

  public setProgress(val: number) {
    if (val > 90) return;
    this.progress = val;
  }

  public start(time = 1000) {
    this.isError = false;
    this.progress = 0;
    let max = 20;
    let step = time / max;
    clearInterval(this.timerProgress);
    this.timerProgress = setInterval(() => {
      if (this.progress >= max) {
        clearInterval(this.timerProgress);
      }
      this.progress += 1;
    }, step);
  }

  public reset() {
    clearInterval(this.timerProgress);
    this.progress = 0;
    this.isError = false;
  }

  public finishedsmooth(time: number) {
    this.isError = false;
    let max = 100;
    let step = time / max;
    clearInterval(this.timerProgress);
    this.timerProgress = setInterval(() => {
      if (this.progress >= max) {
        clearInterval(this.timerProgress);
      }
      this.progress += 1;
    }, step);
  }

  public errorsmooth(time: number) {
    this.isError = true;
    let max = 100;
    let step = time / max;
    clearInterval(this.timerProgress);
    this.timerProgress = setInterval(() => {
      if (this.progress >= max) {
        clearInterval(this.timerProgress);
      }
      this.progress += 1;
    }, step);
  }

  public finished() {
    clearInterval(this.timerProgress);
    this.progress = 100;
  }
}
