export class Query {
  [key: string]: any;
  public page: number = 1;
  public limit: number = 10;
  public search: string = '';
  public total: number = 0;
  public time: number = new Date().getTime();
  public isActive: number = -1;

  constructor() {
    try {
      const strPage = localStorage.getItem('page_limit');
      if (strPage) {
        let page_limit = JSON.parse(strPage);
        if (page_limit) this.limit = page_limit;
      }
    } catch (error) {
      this.limit = 10;
    }
  }
}
