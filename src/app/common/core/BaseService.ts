import { HttpClient, HttpEventType, HttpHeaders } from '@angular/common/http';
import { catchError, retry } from 'rxjs/operators';
import { ServiceLocator } from '@app/common/locator.service';
import { JsonResult } from '@app/models/result.model';
import { ToastrService } from 'ngx-toastr';
import { of } from 'rxjs';

export class BaseService {
  private REST_API_SERVER = ''; // environment.API_URL;
  private header: HttpHeaders;
  private httpClient: HttpClient;
  private toastr: ToastrService;
  public progress: number = 0;
  public isErrorHandle: boolean = true;

  public isShowError: boolean = false;

  constructor() {
    this.header = new HttpHeaders().set('Content-type', 'application/json');
    this.httpClient = ServiceLocator.injector.get(HttpClient);
    this.toastr = ServiceLocator.injector.get(ToastrService);
  }

  public valid() {
    this.isErrorHandle = true;
    return this;
  }

  public noValid() {
    this.isErrorHandle = false;
    return this;
  }

  public sendGetRequest() {
    return this.httpClient.get(this.REST_API_SERVER);
  }

  public get(path: string = '', params: any = {}): Promise<JsonResult> {
    this.header = new HttpHeaders().set('Content-type', 'application/json');
    return new Promise((resolve, reject) => {
      try {
        this.httpClient
          .get(this.REST_API_SERVER + path, { params, headers: this.header })
          .pipe(
            catchError((error: any) => {
              console.log('GET', error, path);
              if (this.isErrorHandle) {
                reject(error);
              } else {
                this.isErrorHandle = true;
                const merr = error.error;
                if (merr) {
                  this.toastr.error(merr.message, 'Lỗi rồi');
                  reject(merr);
                } else {
                  this.toastr.error(error.message, 'Lỗi rồi');
                  reject(error);
                }
              }
              return of([]);
            })
          )
          .subscribe((res: any) => {
            // if (!this.isErrorHandle) {
            //   if (res.isOk) {
            //     this.toastr.success(res.message, 'Thành công');
            //   }
            // }
            resolve(<JsonResult>res);
          });
      } catch (error) {
        console.log(error, path);
      }
    });
  }

  public post(path: string = '', body: any): Promise<JsonResult> {
    this.header = new HttpHeaders().set('Content-type', 'application/json');
    return new Promise((resolve, reject) => {
      this.httpClient
        .post(this.REST_API_SERVER + path, body, { headers: this.header })
        .pipe(retry(2))
        .subscribe(
          (res: any) => {
            if (!this.isErrorHandle) {
              this.isErrorHandle = true;
              this.toastr.success(res.message, 'Thành công');
            }
            resolve(<JsonResult>res);
          },
          (error) => {
            console.log('POST', error, path);
            if (this.isErrorHandle) {
              reject(error.error);
            } else {
              this.isErrorHandle = true;
              const merr = error.error;
              if (merr) {
                this.toastr.error(merr.message, 'Lỗi rồi');
                reject(merr);
              } else {
                this.toastr.error(error.message, 'Lỗi rồi');
                reject(error);
              }
            }
          }
        );
    });
  }

  public postFormData(
    path: string = '',
    body: any,
    cb: any
  ): Promise<JsonResult> {
    let formData = new FormData();
    Object.keys(body).forEach((key) => {
      formData.append(key, body[key]);
    });

    this.header = new HttpHeaders().set('Content-type', 'multipart/form-data');
    return new Promise((resolve, reject) => {
      this.httpClient
        .post(this.REST_API_SERVER + path, formData, {
          reportProgress: true,
          observe: 'events',
        })
        .pipe(retry(2))
        .subscribe(
          (event) => {
            if (event.type === HttpEventType.UploadProgress) {
              if (event.total)
                this.progress = Math.round((100 * event.loaded) / event.total);
            } else if (event.type === HttpEventType.Response) {
              let res = <JsonResult>event.body;
              if (!this.isErrorHandle) {
                this.isErrorHandle = true;
                this.toastr.success(res.message, 'Thành công');
              }
              resolve(res);
            }
            cb(event);
          },
          (error) => {
            if (this.isErrorHandle) {
              reject(error);
            } else {
              this.isErrorHandle = true;
              const merr = error.error;
              if (merr) {
                this.toastr.error(merr.message, 'Lỗi rồi');
                reject(merr);
              } else {
                this.toastr.error(error.message, 'Lỗi rồi');
                reject(error);
              }
            }
          }
        );
    });
  }
}
