import { HttpClient } from '@angular/common/http';
import { Injectable, Injector, PLATFORM_ID } from '@angular/core';
import { Meta, Title } from '@angular/platform-browser';
import { ActivatedRoute, NavigationEnd, Router } from '@angular/router';
import { DOCUMENT, isPlatformBrowser } from '@angular/common';
import { ServiceLocator } from '../locator.service';
import { ToastrService } from 'ngx-toastr';
import { BlockService } from '@base/control/base-block/block.service';
import { Query } from '@base/models/query.model';
import { DialogService } from '@base/control/base-dialog/dialog.service';
import DateHelper from '@base/helper/date.helper';
import { checkPermission } from '@common/common';
import { User } from '@app/models/user.model';
import { AuthService } from '@app/share/services/auth.service';

declare var bootstrap: any;
@Injectable({ providedIn: 'root' })
export class BaseComponent {
  public API_URL: string = '';
  public STATIC_URL: string = '';
  public schema: any;
  public expandFilter: boolean = false;
  public user: User = new User();
  protected route: ActivatedRoute;
  protected title: Title;
  protected http: HttpClient;
  protected router: Router;
  public dom;
  public platform: Object;
  public isPageOverrideSeo: boolean = false;
  public isBrowser: boolean;
  protected _albums = [];
  public toastr: ToastrService;
  public blockService: BlockService;
  public query: Query = new Query();
  public alert: DialogService;
  public authService: AuthService;

  constructor(private injectorObj?: Injector) {
    if (this.injectorObj) {
      this.route = this.injectorObj.get(ActivatedRoute);
      this.title = this.injectorObj.get(Title);
      this.http = this.injectorObj.get(HttpClient);
      this.router = this.injectorObj.get(Router);
      this.dom = this.injectorObj.get(DOCUMENT);
      this.platform = this.injectorObj.get(PLATFORM_ID);
      this.toastr = this.injectorObj.get(ToastrService);
      this.blockService = this.injectorObj.get(BlockService);
      this.alert = this.injectorObj.get(DialogService);
      this.authService = this.injectorObj.get(AuthService);
      this.isBrowser = isPlatformBrowser(this.platform);
    } else {
      this.injectorObj = ServiceLocator.injector;
      this.route = ServiceLocator.injector.get(ActivatedRoute);
      this.title = ServiceLocator.injector.get(Title);
      this.http = ServiceLocator.injector.get(HttpClient);
      this.router = ServiceLocator.injector.get(Router);
      this.dom = ServiceLocator.injector.get(DOCUMENT);
      this.platform = ServiceLocator.injector.get(PLATFORM_ID);
      this.toastr = ServiceLocator.injector.get(ToastrService);
      this.blockService = ServiceLocator.injector.get(BlockService);
      this.alert = ServiceLocator.injector.get(DialogService);
      this.authService = ServiceLocator.injector.get(AuthService);
      this.isBrowser = isPlatformBrowser(this.platform);
    }

    this.user = this.authService.currentUser();
  }
  //#endregion

  public setTitle(title: string) {
    this.title.setTitle(title);
  }

  saveStorageFilter(values: Query) {
    let key = 'filter' + window.location.pathname.replace(/\//g, '_');
    window.localStorage.setItem(key, JSON.stringify(values));
  }

  getStorageFilter() {
    try {
      let key = 'filter' + window.location.pathname.replace(/\//g, '_');
      var variable = window.localStorage.getItem(key);
      if (variable != null && variable != '') {
        let filter = JSON.parse(variable);
        if (!filter['fromDate'] || !filter['toDate']) {
          filter['fromDate'] = DateHelper.dateStart(new Date());
          filter['toDate'] = DateHelper.dateEnd(new Date());
        }
        return filter;
      }
    } catch (error) {
      return null;
    }
    return null;
  }

  hideModal(id: string) {
    const myModalEl = document.getElementById(id);
    const modal = bootstrap.Modal.getInstance(myModalEl);
    if (modal) modal.hide();
  }
  openModal(id: string) {
    const myModalEl = document.getElementById(id);
    let modal = bootstrap.Modal.getInstance(myModalEl);
    if (!modal) modal = new bootstrap.Modal(myModalEl);
    modal.show();
  }

  checkPermission(page: string, act: string, functions: string = '') {
    return checkPermission(page, act, functions);
  }
}
