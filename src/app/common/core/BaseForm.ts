import { EventEmitter, Injectable, Output, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { User } from '@app/models/user.model';
import { AuthService } from '@app/share/services/auth.service';
import { ToastrService } from 'ngx-toastr';
import { ServiceLocator } from '../locator.service';
import { DialogService } from '@common/base-control/base-dialog/dialog.service';
import DateHelper from '@common/base-helper/date.helper';

@Injectable({ providedIn: 'root' })
export class BaseForm {
  public isLoading: boolean = false;
  @Output('load') load: EventEmitter<any> = new EventEmitter();
  @Output('unload') unload: EventEmitter<any> = new EventEmitter();
  @Output('done') done: EventEmitter<any> = new EventEmitter();
  public toastr: ToastrService;
  public dialog: DialogService;

  public authService: AuthService;
  public user: User = new User();
  constructor() {
    this.toastr = ServiceLocator.injector.get(ToastrService);
    this.dialog = ServiceLocator.injector.get(DialogService);
    this.authService = ServiceLocator.injector.get(AuthService);
    this.user = this.authService.currentUser();
  }

  getStorageFilter() {
    try {
      let page = window.location.pathname.replace(/\//g, '_');
      let key = 'filter' + page;
      var variable = window.localStorage.getItem(key);
      if (variable != null && variable != '') {
        let filter = JSON.parse(variable);
        if (!filter['fromDate'] || !filter['toDate']) {
          filter['fromDate'] = DateHelper.dateStart(new Date());
          filter['toDate'] = DateHelper.dateEnd(new Date());
        }
        filter['viewUserId'] = window.localStorage.getItem('viewUserId');
        return filter;
      }
    } catch (error) {
      return null;
    }
    return null;
  }

  loadding() {
    this.isLoading = true;
    this.load.emit();
  }

  unloading() {
    this.isLoading = false;
    this.unload.emit();
  }

  edone(data: any) {
    this.isLoading = false;
    this.done.emit(data);
  }
}
