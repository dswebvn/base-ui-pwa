import { HttpClient } from '@angular/common/http';
import { Injectable, Injector, PLATFORM_ID } from '@angular/core';
import { Meta, Title } from '@angular/platform-browser';
import { ActivatedRoute, NavigationEnd, Router } from '@angular/router';
import { DOCUMENT, isPlatformBrowser } from '@angular/common';
import { ServiceLocator } from '../locator.service';
import { ToastrService } from 'ngx-toastr';
import { Query } from '@base/models/query.model';
import { BlockService } from '@base/control/base-block/block.service';
import { DialogService } from '@base/control/base-dialog/dialog.service';

@Injectable({ providedIn: 'root' })
export class BaseBlockComponent {
  public API_URL: string = '';
  public STATIC_URL: string = '';

  protected route: ActivatedRoute;
  protected router: Router;
  public dom;
  public platform: Object;
  public isBrowser: boolean;
  public toastr: ToastrService;
  public blockService: BlockService;
  public query: Query = new Query();
  public dialog: DialogService;

  constructor(private injectorObj?: Injector) {
    if (this.injectorObj) {
      this.route = this.injectorObj.get(ActivatedRoute);
      this.router = this.injectorObj.get(Router);
      this.dom = this.injectorObj.get(DOCUMENT);
      this.platform = this.injectorObj.get(PLATFORM_ID);
      this.toastr = this.injectorObj.get(ToastrService);
      this.blockService = this.injectorObj.get(BlockService);
      this.isBrowser = isPlatformBrowser(this.platform);
      this.dialog = this.injectorObj.get(DialogService);
    } else {
      this.injectorObj = ServiceLocator.injector;
      this.route = ServiceLocator.injector.get(ActivatedRoute);
      this.router = ServiceLocator.injector.get(Router);
      this.dom = ServiceLocator.injector.get(DOCUMENT);
      this.platform = ServiceLocator.injector.get(PLATFORM_ID);
      this.toastr = ServiceLocator.injector.get(ToastrService);
      this.blockService = ServiceLocator.injector.get(BlockService);
      this.dialog = ServiceLocator.injector.get(DialogService);
      this.isBrowser = isPlatformBrowser(this.platform);
    }
  }
  //#endregion
}
