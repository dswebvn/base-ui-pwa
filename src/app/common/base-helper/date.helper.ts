import { ObjectLiteral } from '@base/models/object.model';
import { DateTime } from 'luxon';
export default class DateHelper {
  /**
   * Format for input html5 datetime-local
   */
  static toDateTimeLocal(date: Date, plus: ObjectLiteral | null = null) {
    let dt = DateTime.fromJSDate(date);
    if (plus) {
      dt = dt.plus(plus);
    }
    return dt.toFormat("yyyy-MM-dd'T'HH:mm");
  }

  static toDateLocal(date: Date, plus: ObjectLiteral | null = null) {
    let dt = DateTime.fromJSDate(date);
    if (plus) {
      dt = dt.plus(plus);
    }
    return dt.toFormat('yyyy-MM-dd');
  }

  static toDateLocalEnd(date: Date, plus: ObjectLiteral | null = null) {
    let dt = DateTime.fromJSDate(date);
    if (plus) {
      dt = dt.plus(plus);
    }
    dt = dt.endOf('month');
    dt = dt.endOf('hour');
    return dt.toFormat('yyyy-MM-dd');
  }

  static toDateLocalStart(date: Date, plus: ObjectLiteral | null = null) {
    let dt = DateTime.fromJSDate(date);
    dt = dt.set({ day: 1 });
    if (plus) {
      dt = dt.plus(plus);
    }
    return dt.toFormat('yyyy-MM-dd');
  }

  static timeLocalToDate(strDate: string) {
    return DateTime.fromFormat(strDate, "yyyy-MM-dd'T'HH:mm").toJSDate();
  }

  static strdateLocalToDate(strDate: string) {
    return DateTime.fromFormat(strDate, 'yyyy-MM-dd').toJSDate();
  }
  static strdateToFormat(strDate: string, format: string = 'dd/MM/yyyy') {
    let dt = DateTime.fromFormat(strDate, "yyyy-MM-dd'T'HH:mm");
    dt.setLocale('vi');
    return dt.toFormat(format);
  }
  static dateToFormat(date: Date, format: string = 'dd/MM/yyyy') {
    let dt = DateTime.fromJSDate(date);
    dt = dt.setLocale('vi');
    return dt.toFormat(format);
  }

  static dateStart(date: Date, plus: ObjectLiteral | null = null) {
    let dt = DateTime.fromJSDate(date);
    if (plus) {
      dt = dt.plus(plus);
    }
    dt = dt.set({ day: 1 });
    return dt.toJSDate();
  }

  static dateEnd(date: Date, plus: ObjectLiteral | null = null) {
    let dt = DateTime.fromJSDate(date);
    if (plus) {
      dt = dt.plus(plus);
    }
    dt = dt.endOf('month');
    return dt.toJSDate();
  }

  static diff(fromDate: Date, toDate: Date, dur: any) {
    const end = DateTime.fromJSDate(toDate);
    const start = DateTime.fromJSDate(fromDate);
    return end.diff(start, dur).toObject();
  }
}
