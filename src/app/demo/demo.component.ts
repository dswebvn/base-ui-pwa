import { Component } from '@angular/core';
import { DialogService } from '@common/base-control/base-dialog/dialog.service';
import { FloatService } from '@common/base-control/base-float/float.service';
import { ExcelMap } from '@common/base-model/excel-map.model';
import { BaseComponent } from '@core/BaseComponent';

@Component({
  selector: 'demo',
  templateUrl: './demo.component.html',
  styleUrls: ['./demo.component.scss'],
})
export class DemoComponent extends BaseComponent {
  step: number = 1;
  isLoad: boolean = false;

  public objServiceMaper: ExcelMap[] = [
    {
      param: 'hoten',
      name: 'Họ và tên',
      desc: 'Họ tên quan tâm',
    },
    {
      param: 'tieude3',
      name: 'Tên sản phẩm',
      desc: 'Tên của 1 sản phẩm',
    },
    {
      param: 'tieude4',
      name: 'Số lượng',
      desc: 'Số lượng',
    },
    {
      param: 'diachi',
      name: 'Địa chỉ',
      desc: 'mô tả địa chỉ ',
    },
    {
      param: 'salePrice',
      name: 'Giá khuyến mãi',
      desc: '',
    },
  ];

  constructor(
    private dialogService: DialogService,
    private floatService: FloatService
  ) {
    super();
  }

  ngOnInit() {}

  stepChange(step: number) {
    console.log(step);
  }

  toggleLoad() {
    if (!this.isLoad) {
      this.blockService.load('demo');
      this.isLoad = true;
    } else {
      this.blockService.unload('demo');
      this.isLoad = false;
    }
  }

  showFloat() {
    this.floatService.open('demo_float');
  }

  showNotification() {
    this.toastr.success('Hello world!', 'Toastr fun!');
  }

  showDialog() {
    this.step = this.step + 1;
    this.dialogService
      .confirm(
        'Theo Cục Hàng không, ngày 16-3 xảy ra vụ việc bốn tiếp ',
        'Bạn có chắc thực hiện điều này'
      )
      .then((res: any) => {
        alert('ok');
      })
      .catch(() => {
        alert('xxxx');
      });
  }

  doImport(data: any) {
    console.log(data);
  }
}
