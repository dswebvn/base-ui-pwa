import { UsrPer } from './sec/usr-per.model';

export class User {
  userId: number | any = null;
  refId: string = '';
  userName: string = '';
  fullName: string = '';
  password: string | null = null;
  secondPassword: string | null = null;
  lastedLogin: Date | null = null;
  accessToken: string | null = null;
  refreshToken: string | null = null;
  token: string | null = null;
  email: string = '';
  isActive: boolean = false;
  avatar: string | null = null;
  selected: boolean | null = false;
  permissions: UsrPer[] = [];
  constructor(fullName: string = '') {
    this.fullName = fullName;
  }
}
