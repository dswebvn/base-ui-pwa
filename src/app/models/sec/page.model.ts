import { Module } from './module.model';
import { Permission } from './permission.model';

export class Page {
  pageId: number | null = null;

  pageName: string = '';

  controllerName: string | null = '';

  actionName: string | null = '';

  notes: string | null = '';

  isRunOnStore: boolean = false;

  isCheckSecurity: boolean = false;

  isUserEditableData: boolean = false;

  sortOrder: number = 0;

  functions: any = [];

  isActive: boolean = true;

  module: Module | null = null;

  permissions: Permission[] = [];
}
