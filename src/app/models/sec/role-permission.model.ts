export class RolePermission {
  rolePermissionId: number | null = null;
  roleId: number = 0;
  permissionId: number = 0;
  constructor(permissionId: number) {
    this.permissionId = permissionId;
  }
}
