import { Permission } from './permission.model';

export class Functions {
  functionId: string | null = null;

  functionName: string = '';

  notes: string | null = '';

  sortOrder: number = 0;

  isSpecial: boolean = false;

  isActive: boolean = true;

  permissions: Permission[] = [];

  checked: boolean = false;
}
