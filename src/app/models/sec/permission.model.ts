import { Functions } from './functions.model';
import { Page } from './page.model';

export class Permission {
  permissionId: number | undefined;
  page: Page | null = null;
  function: Functions | null = null;
}
