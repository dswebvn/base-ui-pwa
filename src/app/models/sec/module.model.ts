import { Page } from './page.model';

export class Module {
  moduleId: number | undefined;

  moduleName: string = '';

  notes: string | null = '';

  sortOrder: number = 0;

  isActive: boolean = true;

  pages: Page[] = [];
}
