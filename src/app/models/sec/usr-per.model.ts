export class UsrPer {
  permissionId: number = -1;
  userId: number = -1;
  storeId: number = -1;
  pageId: number = -1;
  functionId: string = '';
  actionName: string = '';
  controllerName: string = '';
}
