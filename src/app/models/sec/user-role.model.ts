import { Role } from './role.model';

export class UserRole {
  userRoleId: number | null = null;

  storeId: number = 1;

  userId: number | null = 0;

  role: Role = new Role();
}
