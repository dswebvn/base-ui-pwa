export class UserEditableData {
  rowId: number | null = null;
  userId: number = -1;
  storeId: number = 1;
  pageId: number = -1;
  userListId: string | null = '';
}
