import { User } from '../user.model';
import { RolePermission } from './role-permission.model';

export class Role {
  roleId: number | null = null;

  storeId: number = -1;

  moduleId: number | undefined;

  roleCode: string = '';

  roleName: string = '';

  notes: string | null = '';

  createdBy: string = '';
  userCreatedBy: User = new User();

  createdDate: Date = new Date();

  modifyBy: string | null = null;

  modifyDate: Date | null = new Date();

  searchString: string = '';

  isActive: boolean = true;

  rolePermissions: RolePermission[] | null = [];
}
