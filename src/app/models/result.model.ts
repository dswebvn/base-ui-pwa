import { ObjectLiteral } from '@base/models/object.model';

export class JsonResult {
  isOk: boolean;
  data: any[] | any = [];
  totalRow: number = 0;
  message: string = '';
  exception: unknown;
  errors: any;

  constructor(
    isOk: boolean = true,
    data: any,
    totalRow: number = 0,
    message: string = '',
    exception: unknown
  ) {
    this.isOk = isOk;
    this.data = data;
    this.totalRow = totalRow;
    this.message = message;
    this.exception = exception;
  }
}

export const VALIDATOR_MAP: ObjectLiteral = {
  'any.required': 'required',
  'number.base': 'phải là số.',
  'string.email': 'email',
  'string.min': `minlength`,
  'number.min': `min`,
  'string.max': `maxlength`,
  'number.max': `max`,
};
