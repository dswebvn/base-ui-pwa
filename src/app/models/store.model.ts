export class Store {
  storeId: number | null | undefined;
  storeCode: string = '';
  storeName: string = '';
  phone: string | null = '';
  fax: string | null = '';
  hotline: string | null = '';
  taxNumber: string | null = '';
  logo: string | null = '';
  website: string | null = '';
  createdBy: string | null = '';
  createdDate: Date = new Date();
  modifyBy: string | null = '';
  modifyDate: Date | null | undefined;
  isMain: boolean = false;
  isActive: boolean = false;
}
