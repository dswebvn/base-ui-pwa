import { Injector, NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ServiceLocator } from '@common/locator.service';
import { ShareModule } from './share/share.module';
import { DemoComponent } from './demo/demo.component';

@NgModule({
  declarations: [AppComponent, DemoComponent],
  imports: [BrowserModule, AppRoutingModule, ShareModule],
  providers: [],
  bootstrap: [AppComponent],
  exports: [
    DemoComponent
  ],
})
export class AppModule {
  constructor(inject: Injector) {
    ServiceLocator.injector = inject;
  }
}
