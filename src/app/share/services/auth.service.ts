import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { JsonResult } from '@app/models/result.model';
import { UsrPer } from '@app/models/sec/usr-per.model';
import { Store } from '@app/models/store.model';
import { User } from '@app/models/user.model';
import { BaseService } from '@core/BaseService';

export enum LoginMode {
  continue = 2,
  error = 0,
  success = 1,
}

@Injectable({ providedIn: 'root' })
export class AuthService extends BaseService {
  private _token: string = '';
  private _currentUser: User | null = null;
  private _stores: Store[] = [];
  private _permissions: UsrPer[] = [];
  private _interruptedUrl: string = '';

  constructor(private _router: Router) {
    super();
    try {
      this._token = <string>localStorage.getItem('simmed_crm_token');
      const strUser = localStorage.getItem('simmed_crm_user');
      const strStore = localStorage.getItem('simmed_crm_store');
      const strPermissions = localStorage.getItem('simmed_crm_permission');
      if (strUser && strUser != '') {
        this._currentUser = JSON.parse(strUser);
      }
      if (strStore && strStore != '') {
        this._stores = JSON.parse(strStore);
      }
      if (strPermissions && strPermissions != '') {
        this._permissions = JSON.parse(strPermissions);
      }
    } catch (error) {
      this._stores = [];
      // console.log(error);
    }
  }

  public get interruptedUrl(): string {
    return this._interruptedUrl;
  }

  public set interruptedUrl(url: string) {
    this._interruptedUrl = url;
    if (!url) {
      this._interruptedUrl = '/';
      localStorage.removeItem('interruptedUrl');
    } else {
      localStorage.setItem('interruptedUrl', url);
    }
  }

  public forgotVerify(body: any) {
    return this.post('/api/auth/forgot-verify', body);
  }

  public register(body: any) {
    return this.post('/api/auth/register', body);
  }
  public checklogin(body: any): Promise<{ mode: any; data: any }> {
    return new Promise((ok, fail) => {
      this.post('/api/auth/check-login', body)
        .then((repData: JsonResult) => {
          try {
            if (repData.data && !repData.data.stores) {
              this._currentUser = <User>repData.data;
              this._token = <string>this._currentUser.accessToken;
              this._permissions = this._currentUser.permissions;

              localStorage.setItem('simmed_crm_token', this._token);
              localStorage.setItem(
                'simmed_crm_user',
                JSON.stringify(this._currentUser)
              );
              localStorage.setItem(
                'simmed_crm_permission',
                JSON.stringify(this._permissions)
              );
              ok({
                mode: LoginMode.success,
                data: repData.data,
              });
            } else {
              this._stores = <Store[]>repData.data.stores;
              localStorage.setItem(
                'simmed_crm_store',
                JSON.stringify(this._stores)
              );
              ok({
                mode: LoginMode.continue,
                data: repData.data,
              });
            }
          } catch (error) {
            fail(error);
          }
        })
        .catch((err) => {
          fail(err);
        });
    });
  }
  public login(body: any): Promise<JsonResult> {
    return new Promise((ok, fail) => {
      this.post('/api/auth/login', body)
        .then((repData: JsonResult) => {
          try {
            this._currentUser = <User>repData.data;
            this._token = <string>this._currentUser.accessToken;
            this._permissions = this._currentUser.permissions;
            localStorage.setItem('simmed_crm_token', this._token);
            localStorage.setItem(
              'simmed_crm_user',
              JSON.stringify(this._currentUser)
            );
            localStorage.setItem(
              'simmed_crm_permission',
              JSON.stringify(this._permissions)
            );
            ok(repData);
          } catch (error) {
            fail(error);
          }
        })
        .catch((err) => {
          fail(err);
        });
    });
  }
  public updateProfile(user: any): Promise<JsonResult> {
    return new Promise((ok, fail) => {
      this.post('/api/auth/update-profile', { user })
        .then((repData: JsonResult) => {
          try {
            this._currentUser = <User>repData.data;
            this._token = <string>this._currentUser.accessToken;
            localStorage.setItem('simmed_crm_token', this._token);
            localStorage.setItem(
              'simmed_crm_user',
              JSON.stringify(this._currentUser)
            );
            ok(repData);
          } catch (error) {
            fail(error);
          }
        })
        .catch((err) => {
          fail(err);
        });
    });
  }

  public stores() {
    return this._stores;
  }

  public permissions() {
    return this._permissions;
  }

  public updatePermission(permissions: UsrPer[]) {
    this._permissions = permissions;
    localStorage.setItem(
      'simmed_crm_permission',
      JSON.stringify(this._permissions)
    );
  }

  public currentUser(): User {
    return <User>this._currentUser;
  }

  public clearToken() {
    this._token = '';
    localStorage.removeItem('simmed_crm_token');
    localStorage.removeItem('simmed_crm_user');
    localStorage.removeItem('simmed_crm_permission');
  }

  public logOut() {
    this.clearToken();

    this._router.navigate(['/auth', 'login']).then(() => {
      // TODO: If Notification (toast) service is present can show successfully Logged out message
    });
  }

  public isAuthenticated(): boolean {
    return !!this._token;
  }

  public getToken() {
    return this._token;
  }
}
