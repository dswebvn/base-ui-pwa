import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ToastrModule } from 'ngx-toastr';
import { HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { BaseControlModule } from '@common/base-control/base-control.module';
import { BaseFormControlModule } from '@common/base-form-control/base-form-control.module';

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    ToastrModule.forRoot({
      timeOut: 3000,
      positionClass: 'toast-top-center',
      enableHtml: true,
      preventDuplicates: true,
      progressBar: true,
      progressAnimation: 'increasing',
      closeButton: true,
    }),
    HttpClientModule,
    BrowserAnimationsModule,
    BaseControlModule,
    BaseFormControlModule,
    HttpClientModule,
  ],
  exports: [
    ToastrModule,
    HttpClientModule,
    BrowserAnimationsModule,
    BaseControlModule,
    BaseFormControlModule,
    HttpClientModule,
  ],
})
export class ShareModule {}
