import { Location } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { JsonResult } from '@app/models/result.model';
import { User } from '@app/models/user.model';
import { AuthService } from '@app/share/services/auth.service';
import { FileJS } from '@common/base-model/file.model';
import { BaseBlockComponent } from '@core/BaseBlockComponent';
import { ToastrService } from 'ngx-toastr';

declare var bootstrap: any;

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss'],
})
export class ProfileComponent extends BaseBlockComponent implements OnInit {
  user: User | undefined;

  constructor(private authService: AuthService, private _location: Location) {
    super();
  }

  ngOnInit(): void {
    // this.user = this.authService.currentUser();
    this.loadProfile();
    // this.toastr.success('Hello world!', 'Toastr fun!');
  }

  backClicked() {
    this._location.back();
  }

  loadProfile() {
    this.blockService.load('profile');
    // this.userService
    //   .findProfile()
    //   .then((rep: JsonResult) => {
    //     this.user = rep.data;
    //   })
    //   .finally(() => {
    //     this.blockService.unload('profile');
    //   });
  }

  onSubmit(frm: NgForm) {
    if (frm.valid) {
      this.blockService.load('register');
      this.authService
        .noValid()
        .updateProfile(this.user)
        .then((res: JsonResult) => {
          this.toastr.success(res.message, 'Thành công');
        })
        .catch((res: JsonResult) => {
          // let errors = res.errors;
          // errors.forEach((err: { type: string | number; context: { key: string | number; }; }) => {
          //   let merror:any = {};
          //   merror[VALIDATOR_MAP[err.type]]= true;
          //   frm.controls[err.context.key].setErrors({errors:merror});
          // });
          // this.toastr.error( res.message,'Lỗi rồi');
        })
        .finally(() => {
          this.blockService.unload('register');
        });
    }
  }

  handleAttach(files: FileJS[] = []) {
    if (files.length > 0 && this.user) {
      const image = files[0];
      this.user.avatar = image.path;
      this.blockService.load('profile');
      this.authService.updateProfile(this.user).finally(() => {
        this.blockService.unload('profile');
      });
    }
  }

  hideModalAvatar() {
    var myModalEl = document.getElementById('modalAvatar');
    var modal = bootstrap.Modal.getInstance(myModalEl);
    if (modal) modal.hide();
  }
}
