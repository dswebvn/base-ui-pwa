import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { BlockService } from '@app/common/base-control/controls/base-block/block.service';
import { JsonResult } from '@app/models/result.model';
import { Store } from '@app/models/store.model';
import { AuthService, LoginMode } from '@app/services/auth.service';
import { BaseComponent } from '@core/BaseComponent';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent extends BaseComponent implements OnInit {
  public username: string = '';
  public password: string = '';
  public isRemember: boolean = false;
  public isChooseStore: boolean = false;
  public stores: Store[] = [];
  public store: Store = new Store();
  public accessToken: string = '';
  constructor() {
    super();
  }

  ngOnInit(): void {}

  onSubmit(frm: NgForm) {
    if (frm.valid) {
      this.blockService.load('login');
      this.authService
        .noValid()
        .checklogin({
          username: this.username,
          password: this.password,
          isRemember: this.isRemember,
        })
        .then(({ mode, data }) => {
          if (mode == LoginMode.success) {
            setTimeout(() => {
              this.router.navigate([this.authService.interruptedUrl]);
            }, 200);
          } else {
            this.stores = data.stores;
            if (this.stores.length > 0) this.store = this.stores[0];
            this.accessToken = data.accessToken;
            this.isChooseStore = true;
          }
        })
        .catch((res: JsonResult) => {})
        .finally(() => {
          this.blockService.unload('login');
        });
    }
  }
  onSubmitLogin(frm: NgForm) {
    if (frm.valid) {
      this.blockService.load('login');
      this.authService
        .noValid()
        .login({
          accessToken: this.accessToken,
          storeId: this.store.storeId,
          isRemember: this.isRemember,
        })
        .then((res) => {
          setTimeout(() => {
            this.router.navigateByUrl(this.authService.interruptedUrl);
          }, 200);
        })
        .catch((res: JsonResult) => {})
        .finally(() => {
          this.blockService.unload('login');
        });
    }
  }
}
