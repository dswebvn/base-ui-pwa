import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { JsonResult, VALIDATOR_MAP } from '@app/models/result.model';
import { User } from '@app/models/user.model';
import { BaseComponent } from '@core/BaseComponent';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss'],
})
export class RegisterComponent extends BaseComponent implements OnInit {
  constructor() {
    super();
  }

  ngOnInit(): void {
    // this.toastr.success('Hello world!', 'Toastr fun!');
  }

  onSubmit(frm: NgForm) {
    if (frm.valid) {
      this.blockService.load('register');
      this.authService
        .noValid()
        .register({ user: this.user })
        .then((res: JsonResult) => {
          this.toastr.success(res.message, 'Thành công');
          setTimeout(() => {
            this.router.navigate(['/auth/']);
          }, 3000);
        })
        .catch((res: JsonResult) => {
          // let errors = res.errors;
          // errors.forEach((err: { type: string | number; context: { key: string | number; }; }) => {
          //   let merror:any = {};
          //   merror[VALIDATOR_MAP[err.type]]= true;
          //   frm.controls[err.context.key].setErrors({errors:merror});
          // });
          // this.toastr.error( res.message,'Lỗi rồi');
        })
        .finally(() => {
          this.blockService.unload('register');
        });
    }
  }
}
