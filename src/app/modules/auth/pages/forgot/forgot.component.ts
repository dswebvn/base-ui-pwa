import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { AuthService } from '@app/share/services/auth.service';
import { BaseBlockComponent } from '@core/BaseBlockComponent';

@Component({
  selector: 'app-forgot',
  templateUrl: './forgot.component.html',
  styleUrls: ['./forgot.component.scss'],
})
export class ForgotComponent extends BaseBlockComponent implements OnInit {
  isVerifyEmail: boolean = false;
  email: string = '';

  constructor(private authService: AuthService) {
    super();
  }

  ngOnInit(): void {}

  onInit() {}

  onSubmit(frm: NgForm) {
    if (frm.valid) {
      this.blockService.load('forgot');
      this.authService
        .forgotVerify({ email: this.email })
        .then((res) => {
          this.isVerifyEmail = true;
        })
        .catch(() => {})
        .finally(() => {
          this.blockService.unload('forgot');
        });
    } else {
    }
  }
}
