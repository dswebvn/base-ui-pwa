# BaseUiPwa

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 16.1.3.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The application will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via a platform of your choice. To use this command, you need to first add a package that implements end-to-end testing capabilities.

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI Overview and Command Reference](https://angular.io/cli) page.

## Hướng dẫn sử dụng

# Tạo mới

- clone từ project base : git clone https://gitlab.com/dswebvn/base-ui-pwa.git
- Edit url Origin sang url orgin project mới
- Thỉnh thoảng merge code từ upstream sang : git merge upstream/master
-

# Clone về

- Clone project đã cấu hình upstream về máy
- Add Origin upstream : https://gitlab.com/dswebvn/base-ui-pwa.git
- Thỉnh thoảng merge code từ upstream sang : git merge upstream/master

Thông báo lỗi `"refusing to merge unrelated histories"` xảy ra khi Git cố gắng hợp nhất hai nhánh không có chung tổ tiên. Nói cách khác, Git không thể xác định cách kết hợp các thay đổi từ hai nhánh vì chúng đã phát triển độc lập. Điều này có thể xảy ra nếu bạn đã sao chép một kho lưu trữ và sau đó thực hiện các thay đổi quan trọng đối với nhánh cục bộ của mình mà không tìm nạp hoặc hợp nhất các bản cập nhật từ kho lưu trữ ngược dòng.

Để giải quyết lỗi này, bạn có một vài lựa chọn:

Đặt lại cứng nhánh cục bộ của bạn về cam kết mới nhất trên kho lưu trữ ngược dòng: Thao tác này sẽ loại bỏ tất cả các thay đổi cục bộ của bạn và thay thế nhánh của bạn bằng phiên bản mới nhất từ ​​kho lưu trữ ngược dòng. Đây là phương án có sức tàn phá cao nhất nên chỉ được sử dụng như là phương sách cuối cùng.

```
git reset --hard upstream/master
```

Đưa nhánh cục bộ của bạn vào kho lưu trữ ngược dòng: Điều này sẽ duy trì các thay đổi cục bộ của bạn đồng thời kết hợp các thay đổi mới nhất từ ​​kho lưu trữ ngược dòng. Tuy nhiên, việc khởi động lại có thể viết lại lịch sử cam kết của bạn, điều này có thể gây khó khăn cho việc cộng tác với những người khác nếu họ đã thực hiện các thay đổi của bạn.

```
git rebase upstream/master
```

Sử dụng cờ: Cờ này yêu cầu Git buộc hợp nhất, mặc dù lịch sử không liên quan. Điều này có thể nguy hiểm vì nó có thể gây ra xung đột và gây khó khăn cho việc theo dõi các thay đổi của bạn.

```
git merge --allow-unrelated-histories upstream/master
```

Lệnh này sẽ tải xuống tất cả các tệp trong `shallow-file` từ kho lưu trữ upstream, bỏ qua các tệp khác.

```
git fetch upstream --update-shallow --shallow-file shallow-file
```

Cờ `--update-shallow` sẽ xóa các tệp đã thay đổi ở cục bộ trước khi tải xuống các tệp mới từ upstream. Điều này có thể hữu ích nếu bạn muốn đảm bảo rằng các tệp trong kho lưu trữ cục bộ của bạn luôn giống với các tệp trong kho lưu trữ upstream.
Cờ `--shallow-update` sẽ không xóa các tệp đã thay đổi ở cục bộ. Điều này có thể hữu ích nếu bạn muốn giữ lại các thay đổi mà bạn đã thực hiện đối với các tệp.
